class ImageAborter {
	constructor() {
		this.onRequest=this.onRequest.bind(this);
	}

	onRequest(request) {
		if (request.resourceType()=="image") {
			request.abort();
			return;
		}

		/*if (request.url().indexOf("facebook")>=0) {
			request.abort();
			return;
		}*/

		/*if (request.url().indexOf("google")>=0) {
			request.abort();
			return;
		}*/

		//console.log("got request in image loader...");
		request.continue();
	}

	async attach(page) {
		this.page=page;
		this.page.on("request",this.onRequest);
		await this.page.setRequestInterception(true);
	}

	async detach() {
		if (this.page) {
			this.page.removeListener("request",this.onRequest);
			await this.page.setRequestInterception(false);
			this.page=null;
		}
	}
}

module.exports=ImageAborter;