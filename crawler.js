const crawlutil=require("./crawlutil.js");
const ImageAborter=require("./ImageAborter.js");

// xyz

function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

async function getLinkedInProfileInfo(crawl, linkedInUrl) {
	let page=await crawl.getPage();

	await page.goto(linkedInUrl);
	await crawlutil.injectJquery(page);
	await crawlutil.scrollThroughPage(page,1000,100);

	let link=await page.evaluate(()=>{
		let u=[];

		$("a").each((i, el)=>{
			let url=$(el).attr("href");
			if (url.startsWith("/company/"))
				u.push(url);
		});

		return u[0];
	});

	let companyUrl;

	if (link) {
		await page.goto("https://www.linkedin.com"+link);
		await crawlutil.injectJquery(page);

		companyUrl=await page.evaluate(()=>{
			return $(".org-top-card a").attr("href");
		});
	}

	return {
		companyUrl: companyUrl
	}
}

async function login(crawl) {
	crawl.log("Logging in...");

	let page=await crawl.getPage();
	await page.goto("https://www.linkedin.com/login");
	await crawlutil.injectJquery(page);

	let args={
		linkedinEmail: crawl.input.linkedinEmail,
		linkedinPassword: crawl.input.linkedinPassword
	};

	await page.evaluate((args)=>{
		$("#username").val(args.linkedinEmail);
		$("#password").val(args.linkedinPassword);
		$(".btn__primary--large").click();
	},args);

	await page.waitForNavigation();
	let url=await page.url();

	if (url!="https://www.linkedin.com/feed/")
		throw new Error("Login didn't work");

	crawl.log("Login seems to work!");
}

module.exports=async function(crawl) {
	let page=await crawl.getPage();
	page.setDefaultTimeout(120000);

	let imageAborter=new ImageAborter();
	await imageAborter.attach(page);
	await login(crawl);
	crawl.progress(20);

	let index=0;
	for (let peopleLink of crawl.input.profileUrls) {
		crawl.log("Fetch: "+peopleLink);
		let info=await getLinkedInProfileInfo(crawl,peopleLink);
		crawl.result({
			companyUrl: info.companyUrl
		});

		index++;
		crawl.progress(20+80*index/crawl.input.profileUrls.length);
	}
};